<?php
require_once 'Database.php';
require_once 'S3.php';

// Descargamos el archivo de S3
$objS3 = new S3();
$objS3->getS3Object();

$objDb = new Database();
$persons = $objDb->query("SELECT firstname, lastname FROM person");

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Guardar logs en S3</title>
</head>

<body>
    <div class="container">
        <h2>Ingrese sus datos</h2>
        <br>
        <form method="POST" action="save.php">
            <div class="mb-3">
                <label for="firstname" class="form-label">Nombre</label>
                <input type="text" name="firstname" class="form-control" id="firstname" autofocus />
            </div>
            <div class="mb-3">
                <label for="lastname" class="form-label">Apellido</label>
                <input type="text" name="lastname" class="form-control" id="lastname" />
            </div>
            <input type='submit' class="btn btn-primary" value='Guardar' />
        </form>

        <?php
        if (count($persons) > 0) {
        ?>
            <br />
            <h2>Lista de personas</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombres</th>
                        <th scope="col">Apellidos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($persons as $key => $person) {
                    ?>

                        <tr>
                            <th scope="row"><?php echo $key + 1 ?></th>
                            <td><?php echo utf8_decode($person["firstname"]) ?></td>
                            <td><?php echo utf8_decode($person["lastname"]) ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        ?>
    </div>
</body>