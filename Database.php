<?php
require_once 'vendor/autoload.php';
require_once 'config.php';
require_once 'Temp.php';

class Database
{
    private $cn;
    private $objTemp;

    function __construct()
    {
        $this->objTemp = new Temp();
    }

    private function connect()
    {
        global $DB_HOST, $DB_NAME, $DB_USER, $DB_PASSWORD;

        $this->cn = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);

        $this->objTemp->makeLog("Usuario " . $DB_USER . " conectado correctamente a la base de datos");

        if (!$this->cn) {
            $this->objTemp->makeLog("Conexión a la base de datos fallida: " . mysqli_connect_error());
            die("Conexión fallida: " . mysqli_connect_error());
        }
    }

    private function close()
    {
        $this->objTemp->makeLog("Conexión a base de datos cerrada");
        mysqli_close($this->cn);
    }

    public function insert($sql)
    {
        $this->connect();

        $result = mysqli_query($this->cn, $sql);

        $this->objTemp->makeLog("Comando INSERT ejecutado. " . $sql);

        $this->close();

        return $result;
    }

    public function query($sql)
    {
        $this->connect();

        $result = mysqli_query($this->cn, $sql);

        $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

        $this->objTemp->makeLog("Comando QUERY ejecutado." . $sql);

        $this->close();

        return $data;
    }
}
