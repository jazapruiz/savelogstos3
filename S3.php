<?php
require_once 'vendor/autoload.php';
require_once 'config.php';
require_once 'Temp.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class S3
{
    public function s3UploadPutObject()
    {
        global $AWS_REGION,
            $AWS_SCHEME,
            $AWS_VERSION,
            $AWS_ACCESS_KEY,
            $AWS_SECRET_KEY,
            $AWS_BUCKET,
            $TEMP_LOG,
            $LOG_NAME;

        $options = [
            "region" => $AWS_REGION,
            'scheme' => $AWS_SCHEME,
            "version" => $AWS_VERSION,
            "credentials" => [
                "key" => $AWS_ACCESS_KEY,
                "secret" => $AWS_SECRET_KEY,
            ]
        ];

        $file_name = $LOG_NAME;
        $tmp_file = $TEMP_LOG;

        try {
            $s3Client = new S3Client($options);

            $result = $s3Client->putObject([
                "Bucket" => $AWS_BUCKET,
                "Key" => $file_name,
                "SourceFile" => $tmp_file
            ]);

            if ($result->get("@metadata")["statusCode"] === 200) {
                $objTemp = new Temp();
                $objTemp->deleteLog();
            }
        } catch (S3Exception $e) {
            die($e->getMessage());
        }
    }

    public function getS3Object()
    {
        global $AWS_REGION,
            $AWS_SCHEME,
            $AWS_VERSION,
            $AWS_ACCESS_KEY,
            $AWS_SECRET_KEY,
            $AWS_BUCKET,
            $LOG_NAME;


        $options = [
            "region" => $AWS_REGION,
            'scheme' => $AWS_SCHEME,
            "version" => 'latest',
            // "version" => $AWS_VERSION,
            "credentials" => [
                "key" => $AWS_ACCESS_KEY,
                "secret" => $AWS_SECRET_KEY,
            ]
        ];


        try {
            $s3Client = new S3Client($options);

            $result = $s3Client->getObject([
                'Bucket' => $AWS_BUCKET,
                'Key'    => $LOG_NAME
            ]);

            $objTemp = new Temp();

            $objTemp->deleteLog();

            $objTemp->makeLog($result['Body']);
        } catch (S3Exception $e) {
            echo "<div class=\"alert alert-danger\" role=\"alert\">Hubo un erro al recuperar el log.txt de su Bucket S3</div>";
        }
    }
}
