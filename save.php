<?php

require_once 'Database.php';
require_once 'S3.php';

if (empty($_POST['firstname']) && empty($_POST['lastname'])) {
    header('location: index.php');
} else {
    $firstname = addslashes(ucwords(utf8_encode($_POST['firstname'])));
    $lastname = addslashes(ucwords(utf8_encode($_POST['lastname'])));

    $sql = "INSERT INTO person (firstname, lastname) VALUES ('" . $firstname . "', '" . $lastname . "')";

    $objDb = new Database();
    $result = $objDb->insert($sql);

    if ($result) {
        // Subimos el archivo a S3
        $objS3 = new S3();
        $objS3->s3UploadPutObject();

        // Redireccionamos al index
        header('location: index.php');
    } else {
?>
        <!DOCTYPE html>
        <html lang="es">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
            <title>Guardar logs en S3</title>
        </head>

        <body>
            <div class="container">
                <div class="alert alert-danger" role="alert">Registro fallido</div>
                <a href="index.php">Regresar</a>
                <?php
                ?>
            </div>
        </body>
<?php
    }
}
