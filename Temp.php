<?php

class Temp
{
    private function makeDir()
    {
        global $TEMP_DIR;

        if (!file_exists($TEMP_DIR)) {
            mkdir($TEMP_DIR, 0777);
        }
    }

    public function makeLog($text)
    {
        global $TEMP_LOG;

        $this->makeDir();

        $newText = date("c") . ": " . $text;

        if (!file_exists($TEMP_LOG)) {
            // Crear log.txt
            $file = fopen($TEMP_LOG, 'w') or die("Se produjo un error al crear el archivo");
            $fileText = <<<_END
$newText
_END;
            fwrite($file, $fileText) or die("No se pudo escribir en el archivo");
            fclose($file);
        } else {
            // Sobreescribir log.txt
            $oldText = file_get_contents($TEMP_LOG);
            $file = fopen($TEMP_LOG, 'w') or die("Se produjo un error al crear el archivo");
            $fileText = <<<_END
$newText
_END;
            fwrite($file, $oldText . PHP_EOL) or die("No se pudo escribir en el archivo");
            fwrite($file, $fileText) or die("No se pudo escribir en el archivo");
            fclose($file);
        }
    }

    public function deleteLog()
    {
        global $TEMP_DIR, $TEMP_LOG;

        @chmod($TEMP_LOG, 0777);

        if (file_exists($TEMP_LOG)) {
            @unlink($TEMP_LOG);
        }

        if (file_exists($TEMP_DIR)) {
            @rmdir($TEMP_DIR);
        }
    }
}
