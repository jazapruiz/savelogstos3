<?php

// AWS
$AWS_ACCESS_KEY = 'AWS_ACCESS_KEY';
$AWS_SECRET_KEY = 'AWS_SECRET_KEY';
$AWS_BUCKET = 'AWS_BUCKET';
$AWS_REGION = 'us-east-2';
$AWS_SCHEME = 'http';
$AWS_VERSION = '2006-03-01';

// DB
$DB_HOST = 'DB_HOST';
$DB_NAME = 'DB_NAME';
$DB_USER = 'DB_USER';
$DB_PASSWORD = 'DB_PASSWORD';

// Temp directory
$TEMP_DIR = './temp';
$LOG_NAME = 'log.txt';
$TEMP_LOG = $TEMP_DIR . '/' . $LOG_NAME;
