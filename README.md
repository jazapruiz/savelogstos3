# savelogstos3

Small example in php to upload and download a file aws s3 bucket

## Installation

Install dependency manager [composer](https://getcomposer.org/).

Once composer is installed, run the following command to install the dependencies.

```bash
composer i
```

Now you have to replace the values of YOUR AWS_ACCESS_KEY, YOUR_AWS_SECRET_KEY and YOUR_AWS_BUCKET with the correct accesses of your aws bucket. Also replace the values of DB_HOST, DB_NAME, DB_USER, DB_PASSWORD with the correct accesses of your database
